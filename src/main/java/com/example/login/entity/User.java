package com.example.login.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author 赵旋
 * @since 2021-11-07 22:35:16
 */
@Data
public class User implements Serializable {
    private static final long serialVersionUID = 833591754393283444L;
    
    private Integer id;
    
    private String name;
    
    private String pwd;

}