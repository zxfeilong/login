package com.example.login.controller;

import com.example.login.entity.User;
import com.example.login.service.UserService;
import com.example.login.util.R;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
 * (User)表控制层
 *
 * @author 赵旋
 * @since 2021-11-07 22:35:16
 */
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/queryId")
    public R query(Integer id) {
        return R.succ(userService.queryById(id));
    }
    @GetMapping("/queryName")
    public R queryByName(String name){
        User user=userService.queryByName(name);
        System.out.println(user);
        return R.succ(userService.queryByName(name));
    }

}