package com.example.login.util;

import lombok.Data;

import java.io.Serializable;

/**
 * Restful 风格
 */
@Data
public class R implements Serializable {

    private  int code;
    private  String msg;
    private Object data;

    public  static  R succ(int code,String msg,Object data){
        R r=new R();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }
    public  static  R succ(Object data){
        return R.succ(200,"操作成功",data);
    }
    public static R fail(int code,String msg,Object data){
        R r=new R();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }
    public static R fail(String msg){
        return R.fail(400,msg,null);
    }

}
